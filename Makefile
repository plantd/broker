PROJECT = broker
REGISTRY = registry.gitlab.com/plantd/$(PROJECT)
PREFIX ?= /usr
DESTDIR ?=
CONFDIR = /etc
SYSTEMDDIR = /lib/systemd

M := $(shell printf "\033[34;1m▶\033[0m")
TAG := $(shell git describe --all | sed -e's/.*\///g')
VERSION := $(shell git describe)
IMAGE_TAG ?= latest

all: build

-include build/examples.mk

deps: ; $(info $M Installing dependencies)
	@./tools/install-deps

lint: ; $(info $M Linting the files)
	@./tools/checks lint

test: ; $(info $M Running unittests)
	@./tools/checks test

race: ; $(info $M Running data race detector)
	@./tools/checks race

msan: ; $(info $M Running memory sanitizer)
	@./tools/checks msan

coverage: ; $(info $M Generating global code coverage report)
	@./tools/coverage

coverhtml: ; $(info $M Generating global code coverage report in HTML)
	@./tools/coverage html

build: ; $(info $(M) Building project...)
	@go build -o target/plantd-$(PROJECT) -ldflags "-X main.VERSION=$(VERSION)" ./cmd/broker

serve-backend: build ; $(info $(M) Running backend with hot-reload)
	@air

serve-frontend: ; $(info $(M) Running frontend with hot-reload)
	@yarn --cwd=app yarn serve

image: ; $(info $(M) Building application image...)
	@docker build -t $(REGISTRY):$(IMAGE_TAG) -f ./build/Dockerfile .

container: image ; $(info $(M) Running application container...)
	@docker run -p 4041-4045:4041-4045 -p 7200:7200 $(REGISTRY):latest

install-files: ; $(info $(M) Installing plantd $(PROJECT) service...)
	@install -Dm 755 target/plantd-$(PROJECT) "$(DESTDIR)$(PREFIX)/bin/plantd-$(PROJECT)"
	@install -Dm 644 README.md "$(DESTDIR)$(PREFIX)/share/doc/plantd/$(PROJECT)/README"
	@install -Dm 644 LICENSE "$(DESTDIR)$(PREFIX)/share/licenses/plantd/$(PROJECT)/COPYING"
	@mkdir -p "$(CONFDIR)/plantd"
	@mkdir -p /run/plantd
	@install -Dm 644 configs/$(PROJECT).yaml "$(DESTDIR)$(CONFDIR)/plantd/$(PROJECT).yaml"
	@install -Dm 644 init/plantd-$(PROJECT).service "$(DESTDIR)$(SYSTEMDDIR)/system/plantd-$(PROJECT).service"

install: install-files; $(info $(M) Restarting daemon...)
	@systemctl daemon-reload

uninstall: ; $(info $(M) Uninstalling plantd $(PROJECT) service...)
	@rm $(DESTDIR)$(PREFIX)/bin/plantd-$(PROJECT)

clean: ; $(info $(M) Removing generated files... )
	@rm -rf target/

.PHONY: all lint test race msan coverage coverhtml build build-deb image container install-files install uninstall clean
