import "babel-polyfill";
import Vue from "vue";
import VueRouter from "vue-router";
import vuetify from "@/plugins/vuetify";

import "@/styles/main.scss";

import App from "@/App.vue";
import routes from "@/routes";

Vue.use(VueRouter);

const router = new VueRouter({
  routes,
  linkActiveClass: "active",
  mode: "history",
});

new Vue({
  el: "#app",
  vuetify,
  render: (h) => h(App),
  router,
});
