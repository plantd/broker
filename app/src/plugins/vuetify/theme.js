import colors from "vuetify/lib/util/colors";

export const dark = {
  primary: colors.blueGrey.lighten2,
  secondary: colors.blueGrey.lighten4,
  accent: colors.blueGrey.lighten5,
  error: colors.orange.accent3,
  info: colors.cyan.accent4,
  success: colors.green.accent3,
  warning: colors.amber.accent2,
  background: colors.blueGrey.lighten5,
};
