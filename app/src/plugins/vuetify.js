import Vue from "vue";
import Vuetify from "vuetify/lib";
import "@mdi/font/css/materialdesignicons.css";

import { dark } from "./vuetify/theme";

Vue.use(Vuetify);

const opts = {
  icons: {
    iconfont: "mdi",
  },
  theme: {
    dark: true,
    themes: { dark },
  },
};

export default new Vuetify(opts);
