"use strict";

const path = require("path");

const VueLoaderPlugin = require("vue-loader/lib/plugin");
const { VuetifyLoaderPlugin } = require("vuetify-loader");
const HtmlPlugin = require("html-webpack-plugin");
const MiniCSSExtractPlugin = require("mini-css-extract-plugin");

const helpers = require("./helpers");
const isDev = process.env.NODE_ENV === "development";

module.exports = {
  entry: ["babel-polyfill", path.resolve("src", "main")],
  resolve: {
    extensions: [".js", "json", "jsx", "vue"],
    alias: {
      vue$: isDev ? "vue/dist/vue.runtime.js" : "vue/dist/vue.runtime.min.js",
      "@": helpers.root("src"),
    },
  },
  module: {
    rules: [
      {
        test: /\.vue$/i,
        loader: "vue-loader",
        include: [helpers.root("src")],
      },
      {
        test: /\.js$/i,
        loader: "babel-loader",
        include: [helpers.root("src")],
      },
      {
        test: /\.css$/,
        use: [
          isDev ? "vue-style-loader" : MiniCSSExtractPlugin.loader,
          { loader: "css-loader", options: { sourceMap: isDev } },
        ],
      },
      {
        test: /\.sass$/,
        use: [
          isDev ? "vue-style-loader" : MiniCSSExtractPlugin.loader,
          { loader: "css-loader", options: { sourceMap: isDev } },
          {
            loader: "sass-loader",
            options: {
              implementation: require("sass"),
              additionalData: "@import '@/styles/variables.scss'",
              sourceMap: isDev,
            },
          },
        ],
      },
      {
        test: /\.scss$/,
        use: [
          isDev ? "vue-style-loader" : MiniCSSExtractPlugin.loader,
          { loader: "css-loader", options: { sourceMap: isDev } },
          {
            loader: "sass-loader",
            options: {
              implementation: require("sass"),
              additionalData: "@import '@/styles/variables.scss';",
              sourceMap: isDev,
            },
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif|webm|mp4|svg)$/,
        loader: "file-loader",
        options: {
          outputPath: "assets",
          esModule: false,
        },
      },
      { test: /\.(woff|woff2|eot|ttf)(\?.*$|$)/, loader: "file-loader" },
    ],
  },
  plugins: [
    new VueLoaderPlugin(),
    new VuetifyLoaderPlugin(),
    new HtmlPlugin({ template: "index.html" }),
    new MiniCSSExtractPlugin(),
  ],
};
