"use strict";

const webpack = require("webpack");
const { merge } = require("webpack-merge");
const FriendlyErrorsPlugin = require("friendly-errors-webpack-plugin");

const helpers = require("./helpers");
const commonConfig = require("./webpack.common");
const environment = require("./env/dev.env");

const webpackConfig = merge(commonConfig, {
  mode: "development",
  devtool: "eval-cheap-module-source-map",
  output: {
    path: helpers.root("dist"),
    publicPath: "/",
    filename: "js/[name].bundle.js",
    chunkFilename: "js/[id].chunk.js",
  },
  optimization: {
    runtimeChunk: "single",
    splitChunks: {
      chunks: "all",
    },
  },
  plugins: [
    new webpack.EnvironmentPlugin(environment),
    new webpack.HotModuleReplacementPlugin(),
    new FriendlyErrorsPlugin(),
  ],
  devServer: {
    compress: true,
    historyApiFallback: true,
    hot: true,
    open: true,
    overlay: true,
    port: 8080,
    stats: {
      normal: true,
    },
    proxy: {
      "/api": "http://localhost:9080",
    },
  },
});

module.exports = webpackConfig;
