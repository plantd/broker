
<a name="v0.2.4"></a>
## [v0.2.4](https://gitlab.com/plantd/broker/compare/v0.2.3...v0.2.4) (2020-12-17)

### Fix

* switch to debian build image


<a name="v0.2.3"></a>
## [v0.2.3](https://gitlab.com/plantd/broker/compare/v0.2.2...v0.2.3) (2020-12-16)

### Fix

* build pipeline fixes
* improve post-install script
* add postinst script to start service
* improve deb package creation
* path in Makefile

### Merge Requests

* Merge branch 'fix/gitlab-ci' into 'master'
* Merge branch 'fix/Makefile' into 'master'
* Merge branch 'test/gitlab-ci' into 'master'


<a name="v0.2.2"></a>
## [v0.2.2](https://gitlab.com/plantd/broker/compare/v0.2.1...v0.2.2) (2020-05-13)

### Chore

* add previous pipeline steps back in
* remove commented out lines
* documenting comment

### Feat

* pipeline for building deb package

### Fix

* use same yaml file as BBY centrifuge
* tell systemd to make directory for pid file
* comment out debugging
* comment out broken test pipeline
* package path for tests
* build step
* use git tag for version
* docker image name
* stages


<a name="v0.2.1"></a>
## [v0.2.1](https://gitlab.com/plantd/broker/compare/v0.2.0...v0.2.1) (2020-02-28)

### Feat

* improve logging
* remove module from relay messages
* load config file using env
* improve logging
* add environment variables to config

### Fix

* committed wrong rpc after testing
* remove unnecessary warning

### Refactor

* drop mdp types
* replace nonsense logger


<a name="v0.2.0"></a>
## [v0.2.0](https://gitlab.com/plantd/broker/compare/v0.1.9...v0.2.0) (2019-06-05)

### Feat

* add version arg


<a name="v0.1.9"></a>
## [v0.1.9](https://gitlab.com/plantd/broker/compare/v0.1.8...v0.1.9) (2019-05-21)

### Fix

* wrap client calls with mutex


<a name="v0.1.8"></a>
## [v0.1.8](https://gitlab.com/plantd/broker/compare/v0.1.7...v0.1.8) (2019-05-16)

### Feat

* add errors in get properties response
* increase api version

### Fix

* handle short frame messages


<a name="v0.1.7"></a>
## [v0.1.7](https://gitlab.com/plantd/broker/compare/v0.1.6...v0.1.7) (2019-04-30)

### Build

* cleanup lint issues

### Core

* update go-apex version


<a name="v0.1.6"></a>
## [v0.1.6](https://gitlab.com/plantd/broker/compare/v0.1.5...v0.1.6) (2019-04-15)

### Feat

* implement property RPC handlers


<a name="v0.1.5"></a>
## [v0.1.5](https://gitlab.com/plantd/broker/compare/v0.1.4...v0.1.5) (2019-03-31)

### Feat

* update to new API version

### Fix

* update build

### Refactor

* clean up logging


<a name="v0.1.4"></a>
## [v0.1.4](https://gitlab.com/plantd/broker/compare/v0.1.3...v0.1.4) (2019-03-11)

### Build

* link to correct bin


<a name="v0.1.3"></a>
## [v0.1.3](https://gitlab.com/plantd/broker/compare/v0.1.2...v0.1.3) (2019-03-11)

### Build

* create link to tagged bin


<a name="v0.1.2"></a>
## [v0.1.2](https://gitlab.com/plantd/broker/compare/v0.1.1...v0.1.2) (2019-03-11)

### Fix

* force working x/sys module repo


<a name="v0.1.1"></a>
## [v0.1.1](https://gitlab.com/plantd/broker/compare/v0.1.0...v0.1.1) (2019-03-11)

### Build

* fix deps in Dockerfile
* update docker setup
* add gitlab ci file

### Feat

* update unit configuration request
* add ModuleSubmitJob

### Fix

* syntax error in tooling


<a name="v0.1.0"></a>
## [v0.1.0](https://gitlab.com/plantd/broker/compare/0.1.0-beta...v0.1.0) (2019-01-31)

### Feat

* use better config structure

### Refactor

* use golang standards layout


<a name="0.1.0-beta"></a>
## 0.1.0-beta (2019-01-29)

### Core

* use public api
* add new unit config style
* add message handlers to endpoint
* add example client for test
* add license
* initial import

### Feat

* switch configuration to JSON
* implement majordomo protocol
* add gRPC services
* add broker service

### Refactor

* switch to go-kit log
* use mdp api from go-apex
* use log instead of verbose flag
