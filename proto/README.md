# Plantd Protobuf

[Deprecated] shouldn't use this for new development, these have been moved here
to transition away from gRPC and protobuf.

## Develop

Setup `protobuf` compilers and tools

```sh
go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
go get -u github.com/golang/protobuf/protoc-gen-go
go get -u github.com/ckaznocha/protoc-gen-lint
go get -u github.com/pseudomuto/protoc-gen-doc/cmd/protoc-gen-doc
```

Lint with

```sh
./tools/pb-lint
```

If there is an error during this the files will be left in a `lint`
directory, otherwise they are cleaned up.

Generate documentation with

```sh
./tools/pb-docgen
```
