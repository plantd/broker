# Base image: https://hub.docker.com/_/golang/
# create: "docker build -t registry.gitlab.com/plantd/broker:fpm -f .gitlab-ci/fpm.Dockerfile ."
FROM golang:1.13.15-alpine3.12
#MAINTAINER Tom Depew <tom.depew@coanda.ca>

RUN apk update \
    && apk upgrade \
    && apk add --virtual \
        build-dependencies \
        build-base \
        gcc \
        clang \
        pkgconf \
        wget \
        musl-dev \
        git \
        zeromq-dev \
        czmq-dev \
    && apk add \
        bash \
        tar \
        ruby \
        ruby-dev \
        ruby-etc \
    && gem install --no-document \
        fpm
