# Base image: https://hub.docker.com/_/golang/
# create: "docker build -t registry.gitlab.com/plantd/broker:fpm -f .gitlab-ci/fpm.Dockerfile ."
FROM golang:1.13.15-buster
#MAINTAINER Tom Depew <tom.depew@coanda.ca>

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        build-essential \
        apt-utils \
        gcc \
        wget \
        git \
        ruby \
        ruby-dev \
        make \
        libzmq5 \
        libzmq3-dev \
        libczmq4 \
        libczmq-dev \
    && gem install --no-document \
        fpm
