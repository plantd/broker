#!/bin/bash

# This script needs to be called from the repository root.
VERSION="$(git describe | sed -e 's/[vV]//g')" # version number cannot start with char.
DESCRIPTION="Service that functions as the broker for plantd"
LICENSE=$"MIT"
URL=$"https://gitlab.com/plantd/broker"
MAINTAINER=$"Mirko Moeller <mirko.moeller@coanda.ca>"
VENDOR=$""
PKG_NAME="plantd-broker"
INSTALLDIR="/tmp/stage" # Must tell make to put installation files here

if which go; then
  : # colon means do nothing
else
  echo "golang not installed or not reachable"
  exit 1
fi

if which fpm; then
  fpm --input-type dir --output-type deb \
    --name "$PKG_NAME" \
    --version "$VERSION" \
    --package target \
    --description "$DESCRIPTION" \
    --license "$LICENSE" \
    --url "$URL" \
    --maintainer "$MAINTAINER" \
    --vendor "$VENDOR" \
    --after-install tools/post-install.sh \
    --chdir "$INSTALLDIR"
else
  echo "fpm not installed or not reachable"
  exit 1
fi
