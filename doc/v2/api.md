# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [proto/v2/core.proto](#proto/v2/core.proto)
    - [Bus](#plantd.Bus)
    - [Channel](#plantd.Channel)
    - [ChannelRequest](#plantd.ChannelRequest)
    - [ChannelResponse](#plantd.ChannelResponse)
    - [ChannelsResponse](#plantd.ChannelsResponse)
    - [Configuration](#plantd.Configuration)
    - [ConfigurationRequest](#plantd.ConfigurationRequest)
    - [ConfigurationResponse](#plantd.ConfigurationResponse)
    - [ConfigurationsResponse](#plantd.ConfigurationsResponse)
    - [Empty](#plantd.Empty)
    - [Error](#plantd.Error)
    - [Event](#plantd.Event)
    - [EventRequest](#plantd.EventRequest)
    - [EventResponse](#plantd.EventResponse)
    - [EventsResponse](#plantd.EventsResponse)
    - [Job](#plantd.Job)
    - [JobRequest](#plantd.JobRequest)
    - [JobResponse](#plantd.JobResponse)
    - [JobStatusResponse](#plantd.JobStatusResponse)
    - [JobsResponse](#plantd.JobsResponse)
    - [Module](#plantd.Module)
    - [ModuleRequest](#plantd.ModuleRequest)
    - [ModuleResponse](#plantd.ModuleResponse)
    - [ModulesResponse](#plantd.ModulesResponse)
    - [Object](#plantd.Object)
    - [PropertiesRequest](#plantd.PropertiesRequest)
    - [PropertiesResponse](#plantd.PropertiesResponse)
    - [Property](#plantd.Property)
    - [PropertyRequest](#plantd.PropertyRequest)
    - [PropertyResponse](#plantd.PropertyResponse)
    - [Request](#plantd.Request)
    - [Service](#plantd.Service)
    - [Settings](#plantd.Settings)
    - [Settings.PropertiesEntry](#plantd.Settings.PropertiesEntry)
    - [SettingsResponse](#plantd.SettingsResponse)
    - [SettingsResponse.SettingsEntry](#plantd.SettingsResponse.SettingsEntry)
    - [State](#plantd.State)
    - [Status](#plantd.Status)
    - [Status.DetailsEntry](#plantd.Status.DetailsEntry)
    - [StatusRequest](#plantd.StatusRequest)
    - [StatusResponse](#plantd.StatusResponse)
    - [System](#plantd.System)
    - [System.ModulesEntry](#plantd.System.ModulesEntry)
    - [System.ServicesEntry](#plantd.System.ServicesEntry)

    - [Channel.Type](#plantd.Channel.Type)
    - [Configuration.Namespace](#plantd.Configuration.Namespace)
    - [Job.Status](#plantd.Job.Status)


    - [Endpoint](#plantd.Endpoint)


- [Scalar Value Types](#scalar-value-types)



<a name="proto/v2/core.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## proto/v2/core.proto



<a name="plantd.Bus"></a>

### Bus
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| endpoint | [string](#string) |  |  |
| sinks | [Channel](#plantd.Channel) | repeated |  |
| sources | [Channel](#plantd.Channel) | repeated |  |






<a name="plantd.Channel"></a>

### Channel
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| type | [Channel.Type](#plantd.Channel.Type) |  |  |
| endpoint | [string](#string) |  |  |
| envelope | [string](#string) |  |  |






<a name="plantd.ChannelRequest"></a>

### ChannelRequest
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |






<a name="plantd.ChannelResponse"></a>

### ChannelResponse
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| channel | [Channel](#plantd.Channel) |  |  |






<a name="plantd.ChannelsResponse"></a>

### ChannelsResponse
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| channels | [Channel](#plantd.Channel) | repeated |  |






<a name="plantd.Configuration"></a>

### Configuration
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| namespace | [Configuration.Namespace](#plantd.Configuration.Namespace) |  |  |
| name | [string](#string) |  |  |
| properties | [Property](#plantd.Property) | repeated |  |
| objects | [Object](#plantd.Object) | repeated |  |






<a name="plantd.ConfigurationRequest"></a>

### ConfigurationRequest
Used to request a configuration by ID


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| namespace | [Configuration.Namespace](#plantd.Configuration.Namespace) |  |  |
| configuration | [Configuration](#plantd.Configuration) |  |  |






<a name="plantd.ConfigurationResponse"></a>

### ConfigurationResponse
Contains the configuration data requested


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| configuration | [Configuration](#plantd.Configuration) |  |  |






<a name="plantd.ConfigurationsResponse"></a>

### ConfigurationsResponse
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| configurations | [Configuration](#plantd.Configuration) | repeated |  |






<a name="plantd.Empty"></a>

### Empty
An empty message for when no input is needed in an RPC






<a name="plantd.Error"></a>

### Error
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| code | [int32](#int32) |  |  |
| message | [string](#string) |  |  |






<a name="plantd.Event"></a>

### Event
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [int32](#int32) |  |  |
| name | [string](#string) |  |  |
| description | [string](#string) |  |  |






<a name="plantd.EventRequest"></a>

### EventRequest
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  | ID of the module to call |
| event_id | [int32](#int32) |  | Event ID to modify/query |






<a name="plantd.EventResponse"></a>

### EventResponse
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| event | [Event](#plantd.Event) |  |  |






<a name="plantd.EventsResponse"></a>

### EventsResponse
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| events | [Event](#plantd.Event) | repeated |  |






<a name="plantd.Job"></a>

### Job
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| priority | [int32](#int32) |  |  |
| status | [Job.Status](#plantd.Job.Status) |  |  |
| properties | [Property](#plantd.Property) | repeated |  |






<a name="plantd.JobRequest"></a>

### JobRequest
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  | ID of the service to call |
| job_id | [string](#string) |  | Job ID to modify/query |
| job_value | [string](#string) |  | Value of the job to modify/create |
| job_properties | [Property](#plantd.Property) | repeated | Additional properties to help describe the job |






<a name="plantd.JobResponse"></a>

### JobResponse
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| job | [Job](#plantd.Job) |  |  |






<a name="plantd.JobStatusResponse"></a>

### JobStatusResponse
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| job | [Job](#plantd.Job) |  |  |






<a name="plantd.JobsResponse"></a>

### JobsResponse
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| jobs | [Job](#plantd.Job) | repeated |  |






<a name="plantd.Module"></a>

### Module
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| service_name | [string](#string) |  |  |
| configuration_id | [string](#string) |  |  |
| status | [Status](#plantd.Status) |  |  |
| state | [State](#plantd.State) |  |  |
| services | [string](#string) | repeated |  |






<a name="plantd.ModuleRequest"></a>

### ModuleRequest
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| module_id | [string](#string) |  |  |






<a name="plantd.ModuleResponse"></a>

### ModuleResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| module | [Module](#plantd.Module) |  |  |






<a name="plantd.ModulesResponse"></a>

### ModulesResponse
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| modules | [Module](#plantd.Module) | repeated |  |






<a name="plantd.Object"></a>

### Object
Nestable node in a tree


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| type | [string](#string) |  |  |
| properties | [Property](#plantd.Property) | repeated |  |
| objects | [Object](#plantd.Object) | repeated |  |






<a name="plantd.PropertiesRequest"></a>

### PropertiesRequest
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  | ID of the module |
| properties | [Property](#plantd.Property) | repeated | The requested properties |






<a name="plantd.PropertiesResponse"></a>

### PropertiesResponse
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| properties | [Property](#plantd.Property) | repeated |  |
| error | [Error](#plantd.Error) |  |  |






<a name="plantd.Property"></a>

### Property
Key/Value entry that could probably be replaced by a map


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [string](#string) |  | google.protobuf.Any value = 2; |






<a name="plantd.PropertyRequest"></a>

### PropertyRequest
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  | ID of the module |
| key | [string](#string) |  | The name of the requested property |
| value | [string](#string) |  | The value of the property |






<a name="plantd.PropertyResponse"></a>

### PropertyResponse
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| property | [Property](#plantd.Property) |  |  |






<a name="plantd.Request"></a>

### Request
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |






<a name="plantd.Service"></a>

### Service
Service type used in message responses


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  |  |
| description | [string](#string) |  |  |
| configuration_id | [string](#string) |  |  |
| status | [Status](#plantd.Status) |  |  |
| state | [State](#plantd.State) |  |  |






<a name="plantd.Settings"></a>

### Settings
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| properties | [Settings.PropertiesEntry](#plantd.Settings.PropertiesEntry) | repeated | TODO: find out how to make V generic |






<a name="plantd.Settings.PropertiesEntry"></a>

### Settings.PropertiesEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [string](#string) |  |  |






<a name="plantd.SettingsResponse"></a>

### SettingsResponse
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| settings | [SettingsResponse.SettingsEntry](#plantd.SettingsResponse.SettingsEntry) | repeated | TODO: use Any or Oneof |






<a name="plantd.SettingsResponse.SettingsEntry"></a>

### SettingsResponse.SettingsEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [string](#string) |  |  |






<a name="plantd.State"></a>

### State
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [int32](#int32) |  |  |
| name | [string](#string) |  |  |
| description | [string](#string) |  |  |






<a name="plantd.Status"></a>

### Status
Service specific status message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| enabled | [bool](#bool) |  | TODO: consider a different data type, protobuf-c sucks at bool types

/ `true` when service will start on boot |
| loaded | [bool](#bool) |  | `true` when configuration has been read in, or connected to owner |
| active | [bool](#bool) |  | `true` when service is running and operational |
| details | [Status.DetailsEntry](#plantd.Status.DetailsEntry) | repeated | additional optional details |






<a name="plantd.Status.DetailsEntry"></a>

### Status.DetailsEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [string](#string) |  |  |






<a name="plantd.StatusRequest"></a>

### StatusRequest
TODO: document message






<a name="plantd.StatusResponse"></a>

### StatusResponse
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [Status](#plantd.Status) |  |  |






<a name="plantd.System"></a>

### System
TODO: document message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| master | [Service](#plantd.Service) |  |  |
| broker | [Service](#plantd.Service) |  |  |
| services | [System.ServicesEntry](#plantd.System.ServicesEntry) | repeated |  |
| modules | [System.ModulesEntry](#plantd.System.ModulesEntry) | repeated |  |






<a name="plantd.System.ModulesEntry"></a>

### System.ModulesEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [Module](#plantd.Module) |  |  |






<a name="plantd.System.ServicesEntry"></a>

### System.ServicesEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [Service](#plantd.Service) |  |  |








<a name="plantd.Channel.Type"></a>

### Channel.Type
FIXME: type is the wrong word here, too much glib conflict

| Name | Number | Description |
| ---- | ------ | ----------- |
| SOURCE | 0 |  |
| SINK | 1 |  |



<a name="plantd.Configuration.Namespace"></a>

### Configuration.Namespace


| Name | Number | Description |
| ---- | ------ | ----------- |
| ACQUIRE | 0 |  |
| ANALYZE | 1 |  |
| CONTROL | 2 |  |
| EXPERIMENT | 3 |  |
| MONITOR | 4 |  |
| PRESENT | 5 |  |
| RECORD | 6 |  |
| STATE | 7 |  |
| RECIPE | 8 |  |



<a name="plantd.Job.Status"></a>

### Job.Status


| Name | Number | Description |
| ---- | ------ | ----------- |
| SUBMITTED | 0 |  |
| READY | 1 |  |
| STARTED | 2 |  |
| RUNNING | 3 |  |
| PAUSED | 4 |  |
| RESUMED | 5 |  |
| STOPPED | 6 |  |
| FAILED | 7 |  |
| CANCELLED | 8 |  |







<a name="plantd.Endpoint"></a>

### Endpoint
Service definition for `plantd` endpoints.

TODO: consider switching from post with body to plain get with {param}

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| GetConfiguration | [Request](#plantd.Request) | [ConfigurationResponse](#plantd.ConfigurationResponse) | Request the configuration currently loaded by a service |
| GetStatus | [Request](#plantd.Request) | [StatusResponse](#plantd.StatusResponse) | Request a service status |
| GetJob | [JobRequest](#plantd.JobRequest) | [JobResponse](#plantd.JobResponse) | Request a job running or queued on a service by ID |
| GetJobs | [Request](#plantd.Request) | [JobsResponse](#plantd.JobsResponse) | Request all jobs running or queued on a service |
| GetJobStatus | [JobRequest](#plantd.JobRequest) | [JobStatusResponse](#plantd.JobStatusResponse) | TODO: implement - or not, could just return in GetJob |
| GetModule | [ModuleRequest](#plantd.ModuleRequest) | [ModuleResponse](#plantd.ModuleResponse) | List module loaded by the service |
| GetModules | [Request](#plantd.Request) | [ModulesResponse](#plantd.ModulesResponse) | List all modules seen by the service |
| GetSettings | [Request](#plantd.Request) | [SettingsResponse](#plantd.SettingsResponse) | Request the settings held by a service |
| GetActiveJob | [Request](#plantd.Request) | [JobResponse](#plantd.JobResponse) | Request the currently running job on a service |
| CancelJob | [JobRequest](#plantd.JobRequest) | [JobResponse](#plantd.JobResponse) | Cancel a job running or queued on a service |
| SubmitJob | [JobRequest](#plantd.JobRequest) | [JobResponse](#plantd.JobResponse) | Submit a new job to a service |
| SubmitEvent | [EventRequest](#plantd.EventRequest) | [EventResponse](#plantd.EventResponse) | Submit a new event to a service |
| AvailableEvents | [Request](#plantd.Request) | [EventsResponse](#plantd.EventsResponse) | List all events a service can handle to |
| GetProperty | [PropertyRequest](#plantd.PropertyRequest) | [PropertyResponse](#plantd.PropertyResponse) | Request a single property from a service |
| GetProperties | [PropertiesRequest](#plantd.PropertiesRequest) | [PropertiesResponse](#plantd.PropertiesResponse) | Request a list of properties from a service |
| SetProperty | [PropertyRequest](#plantd.PropertyRequest) | [PropertyResponse](#plantd.PropertyResponse) | Submit a change for a single property of a service |
| SetProperties | [PropertiesRequest](#plantd.PropertiesRequest) | [PropertiesResponse](#plantd.PropertiesResponse) | Submit a change for multiple properties of a service |





## Scalar Value Types

| .proto Type | Notes | C++ Type | Java Type | Python Type |
| ----------- | ----- | -------- | --------- | ----------- |
| <a name="double" /> double |  | double | double | float |
| <a name="float" /> float |  | float | float | float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long |
| <a name="bool" /> bool |  | bool | boolean | boolean |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str |
