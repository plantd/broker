package main

import (
	"sync"

	"gitlab.com/plantd/broker/pkg/mdp"

	log "github.com/sirupsen/logrus"
)

func main() {
	log.SetLevel(log.TraceLevel)

	wg := &sync.WaitGroup{}
	worker1, _ := mdp.NewWorker("tcp://localhost:5555", "echo")
	worker2, _ := mdp.NewWorker("tcp://localhost:5555", "echo")

	wg.Add(2)

	go func() {
		defer wg.Done()

		var err error
		var request, reply []string

		for {
			request, err = worker1.Recv(reply)
			log.Debug("worker 1 serving request")
			if err != nil {
				break
			}
			reply = request
		}

		log.Println(err)
	}()

	go func() {
		defer wg.Done()

		var err error
		var request, reply []string

		for {
			request, err = worker2.Recv(reply)
			log.Debug("worker 2 serving request")
			if err != nil {
				break
			}
			reply = request
		}

		log.Println(err)
	}()

	wg.Wait()
}
