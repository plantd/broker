package main

import (
	"log"

	"gitlab.com/plantd/broker/pkg/bus"
)

func main() {
	mqBus := bus.NewBus(
		"test",
		"foo",
		"@tcp://127.0.0.1:9200",
		"@tcp://127.0.0.1:9201",
		"inproc://foo.test.pipe",
	)
	done := make(chan bool)

	go mqBus.Run(done)

	<-done

	log.Print("closing bus example")
}
