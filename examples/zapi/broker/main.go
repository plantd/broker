package main

import (
	"gitlab.com/plantd/broker/pkg/mdp"

	log "github.com/sirupsen/logrus"
)

func main() {
	log.SetLevel(log.TraceLevel)

	endpoint := "tcp://*:5555"
	broker, err := mdp.NewBroker(endpoint)
	if err != nil {
		log.Printf("failed to create broker instance: %s", err)
	}

	if err = broker.Bind(); err != nil {
		log.Printf("failed to bind to endpoint %s: %s", endpoint, err)
	}

	done := make(chan bool, 1)
	go broker.Run(done)
	<-done

	if err = broker.Close(); err != nil {
		log.Printf("failed to close broker endpoint connection: %s", err)
	}
}
