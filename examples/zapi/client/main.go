package main

import (
	"gitlab.com/plantd/broker/pkg/mdp"

	log "github.com/sirupsen/logrus"
)

func main() {
	log.SetLevel(log.TraceLevel)

	client, err := mdp.NewClient("tcp://localhost:5555")
	if err != nil {
		panic(err)
	}

	done := make(chan bool, 1)
	var count int

	go (func() {
		for count = 0; count < 5000; count++ {
			var msg []string
			if err = client.Send("echo", "ping"); err != nil {
				log.WithFields(log.Fields{"err": err}).Error("send")
				return
			}
			if msg, err = client.Recv(); err != nil {
				log.WithFields(log.Fields{"err": err}).Error("recv")
				return
			}
			log.Println(msg)
		}
		done <- true
	})()

	<-done

	log.Printf("%d replies received\n", count)
}
