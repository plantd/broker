package main

import (
	"fmt"
	"log"
	"os"

	czmq "github.com/zeromq/goczmq/v4"
)

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		help()
		log.Fatal("request type required")
	}

	switch request := args[0]; request {
	case "shutdown", "stop", "kill":
		handle("shutdown")
	case "restart":
		handle("restart")
	default:
		help()
	}
}

func help() {
	message := "usage: request <command>\n\n" +
		"available commands:\n" +
		"- shutdown | stop | kill\n" +
		"- restart\n\n"
	fmt.Print(message)
}

func handle(request string) {
	// Create a dealer channeler and connect it to the router.
	dealer := czmq.NewDealerChanneler("tcp://127.0.0.1:5678")
	defer dealer.Destroy()

	dealer.SendChan <- [][]byte{[]byte(request)}

	// Receive the reply.
	reply := <-dealer.RecvChan
	log.Printf("broker response: '%v'", reply[0])
}
