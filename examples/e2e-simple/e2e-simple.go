package main

import (
	"bytes"
	"context"
	"crypto/rand"
	"errors"
	"math/big"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/plantd/broker/pkg/mdp"
	pb "gitlab.com/plantd/broker/pkg/proto/v1"

	"github.com/golang/protobuf/jsonpb"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/connectivity"
)

type module struct {
	endpoint string
	service  string
}

type client struct {
	addr string
}

func main() {
	modules := []*module{
		{endpoint: "tcp://localhost:7200", service: "acquire"},
	}

	clients := []*client{
		{addr: "localhost:4041"},
	}

	log.SetLevel(log.DebugLevel)

	ctx, cancelFunc := context.WithCancel(context.Background())

	go run(ctx)

	time.Sleep(time.Second * 1)

	for _, module := range modules {
		go startModule(ctx, module)
	}

	time.Sleep(time.Second * 1)

	for _, client := range clients {
		go startClient(ctx, client)
	}

	termChan := make(chan os.Signal)
	signal.Notify(termChan, syscall.SIGINT, syscall.SIGTERM)
	<-termChan

	cancelFunc()
}

func run(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			log.Println("stopping")
			return
		}
	}
}

// startModule simulates the module device connected to a unit
func startModule(ctx context.Context, m *module) {
	var worker *mdp.Worker
	var err error

	if worker, err = mdp.NewWorker(m.endpoint, m.service); err != nil {
		panic(err)
	}

	log.Printf("starting module: %v\n", m)

	go (func() {
		var request, reply []string
		for {
			log.Println("waiting for request")
			if request, err = worker.Recv(reply); err != nil {
				log.Errorf("%s", err)
			}
			log.Printf("request received: %v\n", request)

			if len(request) == 0 {
				log.Errorf("request has zero parts")
				continue
			}

			msgType := request[0]

			// Reset reply
			reply = []string{}

			for _, part := range request[1:] {
				var data []byte
				switch msgType {
				case "get-module":
					log.Printf("receive request for: %s\n", part)
					response := &pb.ModuleResponse{
						Module: &pb.Module{
							Id:          m.service,
							Name:        m.service + "-test",
							ServiceName: m.service,
						},
					}
					b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
					if err = marshaler.Marshal(&b, response); err != nil {
						log.Errorf("%s\n", err)
						break
					}
					data = b.Bytes()
				default:
					log.Errorf("invalid message type provided: %s", msgType)
				}

				reply = append(reply, string(data))
			}
		}
	})()

	select {
	case <-ctx.Done():
		log.Println("stopping modules")
		return
	}
}

// startClient makes requests over gRPC
func startClient(ctx context.Context, c *client) {
	ready := false
	nRetry := 25

	conn, err := grpc.Dial(c.addr, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}

	for i := 0; i < nRetry; i++ {
		time.Sleep(100 * time.Millisecond)
		if conn.GetState() == connectivity.Ready {
			ready = true
			break
		}
	}

	if !ready {
		panic(errors.New("connection failure"))
	}

	ec := pb.NewEndpointClient(conn)

	go (func() {
		for {
			for i := 0; i < 10; i++ {
				log.Printf("sending request to: %s\n", c.addr)
				getModule(ec, "acquire")
				nRand, _ := rand.Int(rand.Reader, big.NewInt(200))
				n := nRand.Int64()
				time.Sleep(time.Millisecond * time.Duration(100+n))
			}
			time.Sleep(10 * time.Second)
		}
	})()

	select {
	case <-ctx.Done():
		log.Println("stopping clients")
		return
	}
}

func getModule(client pb.EndpointClient, service string) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	request := &pb.ModuleRequest{Id: service}
	var response *pb.ModuleResponse
	var err error

	if response, err = client.GetModule(ctx, request); err != nil {
		log.Errorf("failed: %v\n", err)
	}

	log.Printf("response: %v\n", response)
}
