package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"regexp"
	"sync"
	"syscall"

	gcontext "gitlab.com/plantd/broker/broker/context"
	"gitlab.com/plantd/broker/broker/service"
	"gitlab.com/plantd/broker/broker/state"

	log "github.com/sirupsen/logrus"
)

func main() {
	processArgs()

	config, err := gcontext.LoadConfig()
	if err != nil {
		msg := fmt.Sprintf("an unrecoverable error occurred while loading the configuration: %s", err)
		panic(msg)
	}

	initLogging(config)

	broker := service.NewBroker(config)
	if broker == nil {
		panic("an unrecoverable error occurred while creating the broker")
	}

	ctx, cancelFunc := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}

	wg.Add(3)
	go broker.App(ctx, wg)
	go broker.Serve(ctx, wg)
	go broker.Start(ctx, wg)

	state.SetStatus("online")

	log.Debug("started broker")

	termChan := make(chan os.Signal, 1)
	signal.Notify(termChan, syscall.SIGINT, syscall.SIGTERM)
	<-termChan

	log.Debug("terminated broker")

	cancelFunc()
	wg.Wait()

	log.Debug("all done")
}

func processArgs() {
	if len(os.Args) > 1 {
		r := regexp.MustCompile("^-v$|^(-{2})?version$")
		if r.Match([]byte(os.Args[1])) {
			fmt.Println(VERSION)
		}
		os.Exit(0)
	}
}

func initLogging(config *gcontext.Config) {
	if config.Log.Formatter == "json" {
		log.SetFormatter(&log.JSONFormatter{})
	}

	if level, err := log.ParseLevel(config.Log.Level); err == nil {
		log.SetLevel(level)
	}
}
