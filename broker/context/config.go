package context

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

// Deprecated: This was intended for something that was never implemented.
type configure struct {
	Host   string `mapstructure:"host"`
	Port   int    `mapstructure:"port"`
	UseTLS bool   `mapstructure:"tls"`
	CACert string `mapstructure:"cacert"`
	Cert   string `mapstructure:"cert"`
	Key    string `mapstructure:"key"`
}

// Deprecated: Unit services will be removed in future releases
type unit struct {
	Endpoint string `mapstructure:"endpoint"`
	RPCBind  string `mapstructure:"rpc-bind"`
	RPCPort  int    `mapstructure:"rpc-port"`
}

type bus struct {
	Name     string `mapstructure:"name"`
	Frontend string `mapstructure:"frontend"`
	Backend  string `mapstructure:"backend"`
	Capture  string `mapstructure:"capture"`
}

type log struct {
	Debug     bool   `mapstructure:"debug"`
	Formatter string `mapstructure:"formatter"`
	Level     string `mapstructure:"level"`
}

// Config defines the structure of the application configuration data.
type Config struct {
	Env               string          `mapstructure:"env"`
	Endpoint          string          `mapstructure:"endpoint"`
	ClientEndpoint    string          `mapstructure:"client-endpoint"`
	HeartbeatLiveness int             `mapstructure:"heartbeat-liveness"`
	HeartbeatInterval int             `mapstructure:"heartbeat-interval"`
	Configure         configure       `mapstructure:"configure"`
	Units             map[string]unit `mapstructure:"units"`
	Buses             []bus           `mapstructure:"buses"`
	Log               log             `mapstructure:"log"`
}

// LoadConfig reads in a configuration file from a set of locations and
// deserializes it into a Config instance.
func LoadConfig() (*Config, error) {
	home, err := homedir.Dir()
	if err != nil {
		return nil, err
	}

	config := viper.New()

	file := os.Getenv("PLANTD_BROKER_CONFIG")
	if file == "" {
		config.SetConfigName("broker")
		config.AddConfigPath(".")
		config.AddConfigPath(fmt.Sprintf("%s/.config/plantd", home))
		config.AddConfigPath("/etc/plantd")
	} else {
		var extension string
		base := filepath.Base(file)
		if strings.HasSuffix(base, "yaml") ||
			strings.HasSuffix(base, "json") ||
			strings.HasSuffix(base, "toml") {
			// strip the file type for viper
			parts := strings.Split(filepath.Base(file), ".")
			base = strings.Join(parts[:len(parts)-1], ".")
			extension = parts[len(parts)-1]
		} else {
			return nil, errors.New("configuration does not support that extension type")
		}
		config.SetConfigName(base)
		config.SetConfigType(extension)
		config.SetConfigFile(file)
		config.AddConfigPath(filepath.Dir(file))
	}

	err = config.ReadInConfig()
	if err != nil {
		return nil, err
	}

	config.SetEnvPrefix("PLANTD_BROKER")
	config.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	config.AutomaticEnv()

	var c Config

	err = config.Unmarshal(&c)
	if err != nil {
		return nil, err
	}

	return &c, nil
}
