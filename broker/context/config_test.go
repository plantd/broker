package context

import (
	"os"
	"reflect"
	"testing"
)

func buildConfig(t *testing.T, path string) *Config {
	_ = os.Setenv("PLANTD_BROKER_CONFIG", path)
	config, err := LoadConfig()
	if err != nil {
		t.Fatalf("Cannot create configuration: %v", err)
	}

	return config
}

func TestLoadConfig(t *testing.T) {
	emptyConfig := &Config{
		Env:               "",
		Endpoint:          "",
		HeartbeatLiveness: 0,
		HeartbeatInterval: 0,
		Configure: configure{
			Host:   "",
			Port:   0,
			UseTLS: false,
		},
		Units: nil,
		Log: log{
			Debug:     false,
			Formatter: "",
			Level:     "",
		},
	}
	validConfig := &Config{
		Env:               "testing",
		Endpoint:          "tcp://*:7200",
		ClientEndpoint:    "tcp://localhost:7200",
		HeartbeatLiveness: 3,
		HeartbeatInterval: 2500000,
		Configure: configure{
			Host:   "localhost",
			Port:   4000,
			UseTLS: false,
		},
		Units: map[string]unit{
			"test": {
				Endpoint: "tcp://localhost:7201",
				RPCBind:  "127.0.0.1",
				RPCPort:  4041,
			},
		},
		Log: log{
			Debug:     true,
			Formatter: "text",
			Level:     "debug",
		},
	}
	cases := []struct {
		fixture string
		want    *Config
		name    string
	}{
		{
			fixture: "fixtures/config/empty.yaml",
			want:    emptyConfig,
			name:    "EmptyFixture",
		},
		{
			fixture: "fixtures/config/valid.yaml",
			want:    validConfig,
			name:    "ValidYAMLFixture",
		},
		{
			fixture: "fixtures/config/valid.json",
			want:    validConfig,
			name:    "ValidJSONFixture",
		},
		{
			fixture: "fixtures/config/valid.toml",
			want:    validConfig,
			name:    "ValidTOMLFixture",
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			config := buildConfig(t, tc.fixture)

			// split up data to make it easier to see where an error occurs
			got := map[string]interface{}{
				"env":       config.Env,
				"endpoint":  config.Endpoint,
				"configure": config.Configure,
				"units":     config.Units,
				"log":       config.Log,
			}

			wanted := map[string]interface{}{
				"env":       tc.want.Env,
				"endpoint":  tc.want.Endpoint,
				"configure": tc.want.Configure,
				"units":     tc.want.Units,
				"log":       tc.want.Log,
			}

			for key, value := range wanted {
				if !reflect.DeepEqual(value, got[key]) {
					t.Errorf("Expected: %v, got: %v", wanted[key], got[key])
				}
			}
		})
	}
}
