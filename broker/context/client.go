package context

import (
	"gitlab.com/plantd/broker/pkg/mdp"
)

// ConnectClient connects a broker service client at the configured endpoint
func ConnectClient(config *Config) (*mdp.Client, error) {
	client, err := mdp.NewClient(config.ClientEndpoint)
	if err != nil {
		return nil, err
	}

	return client, nil
}
