package service

import (
	"context"
	"sync"
	"testing"

	pb "gitlab.com/plantd/broker/pkg/proto/v1"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
)

func TestResponseError(t *testing.T) {
	response := &pb.PropertiesResponse{}
	expected := UnexpectedResponseError()
	setResponseError(response, UnexpectedResponseError())
	assert.Equal(t, response.Error.Code, expected.Code)
	assert.Equal(t, response.Error.Message, expected.Message)
}

type MockClient struct {
	mock.Mock
}

func (m *MockClient) Send(name string, request ...string) error {
	args := m.Called(name, request)
	return args.Error(0)
}

func (m *MockClient) Recv() ([]string, error) {
	args := m.Called()
	return args.Get(0).([]string), args.Error(1)
}

type GrpcEndpointTestSuite struct {
	suite.Suite
	client   *MockClient
	endpoint GrpcEndpoint
	request  []string
	reply    []string
}

func (suite *GrpcEndpointTestSuite) SetupTest() {
	suite.client = new(MockClient)
	suite.endpoint = GrpcEndpoint{
		name:   "test",
		client: suite.client,
		mutex:  &sync.Mutex{},
	}
	suite.request = make([]string, 2)
	suite.reply = make([]string, 1)
}

func TestGrpcEndpointTestSuite(t *testing.T) {
	suite.Run(t, new(GrpcEndpointTestSuite))
}

func (suite *GrpcEndpointTestSuite) TestGrpcEndpoint_GetChannel() {
	suite.request[0] = GetChannel
	suite.request[1] = `{"id":"test"}`
	suite.reply[0] = `{
		"channel": {
			"type": 0,
			"endpoint": "inproc://test",
			"envelope": "test"
		}
	}`

	suite.client.On("Send", "test", suite.request).Return(nil)
	suite.client.On("Recv").Return(suite.reply, nil)

	response, err := suite.endpoint.GetChannel(context.TODO(), &pb.ChannelRequest{Id: "test"})
	suite.Nil(err, "GetChannel should not return an error")
	suite.NotNil(response, "GetChannel should return a response")
	suite.NotNil(response.Channel, "GetChannel response contains channel data")
	suite.Equal(pb.Channel_SOURCE, response.Channel.Type)
	suite.Equal("inproc://test", response.Channel.Endpoint)
	suite.Equal("test", response.Channel.Envelope)
}

func (suite *GrpcEndpointTestSuite) TestGrpcEndpoint_GetChannels() {

	suite.request[0] = GetChannels
	suite.request[1] = "{}"
	suite.reply[0] = `{
		"channels": [{
			"type": 0,
			"endpoint": "inproc://test",
			"envelope": "test"
		}]
	}`

	suite.client.On("Send", "test", suite.request).Return(nil)
	suite.client.On("Recv").Return(suite.reply, nil)

	response, err := suite.endpoint.GetChannels(context.TODO(), &pb.Empty{})
	suite.Nil(err, "GetChannels should not return an error")
	suite.NotNil(response, "GetChannels should return a response")
	suite.Equal(1, len(response.Channels))
	suite.Equal(pb.Channel_SOURCE, response.Channels[0].Type)
	suite.Equal("inproc://test", response.Channels[0].Endpoint)
	suite.Equal("test", response.Channels[0].Envelope)
}

func (suite *GrpcEndpointTestSuite) TestGrpcEndpoint_GetConfiguration() {
	suite.request[0] = GetConfiguration
	suite.request[1] = `{"id":"test"}`
	suite.reply[0] = `{
		"configuration": {
			"id": "test",
			"namespace": 0,
			"name": "acquire-test",
			"properties": [{
				"key": "key_a", "value": "value_a"
			}, {
				"key": "key_b", "value": "value_b"
			}],
			"objects": [{
				"id": "obj1",
				"name": "object_1",
				"type": "box",
				"properties": [{
					"key": "obj1key_a", "value": "obj1val_a"
				}],
				"objects": [{}]
			}, {
				"id": "obj2",
				"name": "object_2",
				"type": "chart",
				"properties": [{
					"key": "obj2key_a", "value": "obj2val_a"
				}],
				"objects": [{}]
			}]
		}
	}`

	suite.client.On("Send", "test", suite.request).Return(nil)
	suite.client.On("Recv").Return(suite.reply, nil)

	response, err := suite.endpoint.GetConfiguration(context.TODO(), &pb.ConfigurationRequest{Id: "test"})
	suite.Nil(err, "GetConfiguration should not return an error")
	suite.NotNil(response, "GetConfiguration should return a response")
	suite.NotNil(response.Configuration, "GetConfiguration response contains configuration data")
	suite.Equal("test", response.Configuration.Id)
	suite.Equal(pb.Configuration_ACQUIRE, response.Configuration.Namespace)
	suite.Equal("acquire-test", response.Configuration.Name)

	suite.Equal("key_a", response.Configuration.Properties[0].Key)
	suite.Equal("value_a", response.Configuration.Properties[0].Value)
	suite.Equal("key_b", response.Configuration.Properties[1].Key)
	suite.Equal("value_b", response.Configuration.Properties[1].Value)

	suite.Equal("obj1", response.Configuration.Objects[0].Id)
	suite.Equal("object_1", response.Configuration.Objects[0].Name)
	suite.Equal("box", response.Configuration.Objects[0].Type)
	suite.Equal("obj1key_a", response.Configuration.Objects[0].Properties[0].Key)
	suite.Equal("obj1val_a", response.Configuration.Objects[0].Properties[0].Value)

	suite.Equal("obj2", response.Configuration.Objects[1].Id)
	suite.Equal("object_2", response.Configuration.Objects[1].Name)
	suite.Equal("chart", response.Configuration.Objects[1].Type)
	suite.Equal("obj2key_a", response.Configuration.Objects[1].Properties[0].Key)
	suite.Equal("obj2val_a", response.Configuration.Objects[1].Properties[0].Value)
}
