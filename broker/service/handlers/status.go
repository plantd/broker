package handlers

import (
	"fmt"

	"gitlab.com/plantd/broker/broker/state"

	"github.com/gin-gonic/gin"
)

func Status(c *gin.Context) {
	status := fmt.Sprintf(`{"status":"%s"}`, state.GetStatus())
	c.String(200, status)
}
