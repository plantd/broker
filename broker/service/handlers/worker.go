package handlers

import (
	"net/http"
	"strconv"

	"gitlab.com/plantd/broker/pkg/mdp"

	"github.com/gin-gonic/gin"
)

type worker struct {
	Id          int    `json:"id"`
	Name        string `json:"name"`
	Service     string `json:"service"`
	Description string `json:"description"`
}

var mockWorkers = []worker{
	{
		Id:          1,
		Name:        "Foo",
		Service:     "org.plantd.dev.Foo",
		Description: "The first module.",
	},
	{
		Id:          2,
		Name:        "Bar",
		Service:     "org.plantd.dev.Bar",
		Description: "The second module.",
	},
	{
		Id:          3,
		Name:        "Baz",
		Service:     "org.plantd.dev.Baz",
		Description: "The third module.",
	},
}

func GetWorker(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, nil)
	}
	if id < 0 || id > len(mockWorkers) {
		c.JSON(http.StatusNotFound, nil)
	}
	c.JSON(http.StatusOK, mockWorkers[id])
}

func GetWorkers(c *gin.Context) {
	c.JSON(http.StatusOK, mockWorkers)
}

func GetWorkerInfo(c *gin.Context) {
	_mq, _ := c.Get("mq")
	mq := _mq.(*mdp.Broker)
	c.JSON(http.StatusOK, mq.GetWorkerInfo())
}
