package handlers

import (
	"fmt"

	"gitlab.com/plantd/broker/broker/state"

	"github.com/gin-gonic/gin"
)

type errors struct {
	ErrorCount int    `json:"count"`
	LastError  string `json:"last"`
}

func Errors(c *gin.Context) {
	errorInfo := errors{
		ErrorCount: state.GetErrorCount(),
		LastError:  fmt.Sprintf("%s", state.GetLastError()),
	}
	c.JSON(200, errorInfo)
}
