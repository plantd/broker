package service

import (
	"context"
	"sync"
)

// Service defines an interface for message handling services.
type Service interface {
	Start(ctx context.Context, wg *sync.WaitGroup) error
}
