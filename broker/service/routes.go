package service

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/plantd/broker/broker/service/handlers"
)

func initializeRoutes(router *gin.Engine) {
	v1 := router.Group("/api/v1")
	{
		v1.GET("/status", handlers.Status)
		v1.GET("/hello", handlers.Hello)
		v1.GET("/errors", handlers.Errors)
		v1.GET("/workers", handlers.GetWorkers)
		v1.GET("/workers/:id", handlers.GetWorker)
		v1.GET("/info", handlers.GetWorkerInfo)
	}
}
