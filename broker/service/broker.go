package service

import (
	"context"
	"errors"
	"fmt"
	"sync"

	gcontext "gitlab.com/plantd/broker/broker/context"
	"gitlab.com/plantd/broker/broker/state"
	"gitlab.com/plantd/broker/pkg/bus"
	"gitlab.com/plantd/broker/pkg/mdp"

	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	czmq "github.com/zeromq/goczmq/v4"
)

const (
	grpcService = iota + 1
	mdpService
)

// This should come from configuration, but for now only support gRPC
var serviceType = grpcService

// Broker defines a plantd broker instance.
type Broker struct {
	buses    []*bus.Bus
	endpoint string
	mq       *mdp.Broker
	running  bool
	services map[string]Service
}

// NewBroker creates an instance of the broker class.
func NewBroker(config *gcontext.Config) *Broker {
	broker := &Broker{
		buses:    initBuses(config),
		endpoint: config.Endpoint,
		mq:       nil,
		running:  false,
		services: make(map[string]Service),
	}

	if err := broker.init(config); err != nil {
		log.WithFields(log.Fields{"err": err}).Error("failed to initialize")
		return nil
	}

	return broker
}

func initBuses(config *gcontext.Config) (buses []*bus.Bus) {
	for _, b := range config.Buses {
		log.WithFields(log.Fields{
			"bus":      b.Name,
			"backend":  b.Backend,
			"frontend": b.Frontend,
			"capture":  b.Capture,
		}).Info("initializing message bus")
		buses = append(buses, bus.NewBus(b.Name, b.Name, b.Backend, b.Frontend, b.Capture))
	}

	return
}

func (b *Broker) init(config *gcontext.Config) error {
	// prepare units for zmq and gRPC communication
	for name, unit := range config.Units {
		var err error

		log.WithFields(log.Fields{
			"unit":     name,
			"endpoint": unit.Endpoint,
		}).Info("adding service unit")

		switch serviceType {
		case grpcService:
			// message proxies
			if b.services[name], err = NewGrpcService(name, config); err != nil {
				log.WithFields(log.Fields{
					"unit": name,
					"err":  err,
				}).Error("failed to create gRPC service")
				return err
			}
		case mdpService:
			if b.services[name], err = NewMdpService(name, config); err != nil {
				log.WithFields(log.Fields{
					"unit": name,
					"err":  err,
				}).Error("failed to create MDP service")
				return err
			}
		}
	}

	b.mq, _ = mdp.NewBroker(b.endpoint)
	if err := b.mq.Bind(); err != nil {
		log.WithFields(log.Fields{
			"err":      err,
			"endpoint": b.endpoint,
		}).Error("failed to bind to endpoint")
		return err
	}

	return nil
}

func (b *Broker) handleRequest(request [][]byte) {
	log.WithFields(log.Fields{
		"client":  string(request[0]),
		"request": request[1],
	}).Debugf("broker received request")

	switch string(request[1]) {
	case "shutdown", "stop", "kill":
		log.Println("shutdown")
	}
}

// Start is responsible for bootstrapping the worker threads and handling shutdown.
func (b *Broker) Start(ctx context.Context, wg *sync.WaitGroup) {
	defer wg.Done()

	wg.Add(len(b.services))
	for _, service := range b.services {
		s := service
		go func() {
			if err := s.Start(ctx, wg); err != nil {
				log.Error(err)
			}
		}()
	}

	wg.Add(len(b.buses))
	for _, item := range b.buses {
		elem := item
		go func() {
			if err := elem.Start(ctx, wg); err != nil {
				log.Error(err)
			}
		}()
	}

	done := make(chan bool, 1)
	go b.mq.Run(done)

	state.SetLastError(errors.New("something"))
	log.Debug("starting broker")
	for {
		var err error
		var event mdp.Event
		log.Debug("waiting for message")
		select {
		case event = <-b.mq.EventChannel:
			log.Debug(event)
		case err = <-b.mq.ErrorChannel:
			state.SetLastError(err)
			log.WithFields(log.Fields{"error": err}).Error("received error from message queue")
		case <-ctx.Done():
			_ = b.mq.Close()
			done <- true
			log.Debug("exiting broker")
			return
		}
	}
}

// Serve launches a simple REQ/REP service to handler basic messages.
func (b *Broker) Serve(ctx context.Context, wg *sync.WaitGroup) {
	defer wg.Done()

	log.Debug("starting request service")

	router := czmq.NewRouterChanneler("tcp://*:5678")
	defer router.Destroy()

	for {
		log.Debug("waiting for request")
		var request [][]byte

		select {
		case request = <-router.RecvChan:
			b.handleRequest(request)
		case <-ctx.Done():
			log.Debug("exiting request service")
			return
		}

		router.SendChan <- [][]byte{request[0], {200}}
	}
}

// App creates a web application to serve static website content.
func (b *Broker) App(ctx context.Context, wg *sync.WaitGroup) {
	defer wg.Done()

	staticContents := "./public"
	bindAddress := "0.0.0.0"
	bindPort := 9080

	go func() {
		gin.SetMode(gin.ReleaseMode)
		r := gin.New()

		r.Use(static.Serve("/", static.LocalFile(staticContents, false)))
		r.Use(gin.Recovery())
		r.Use(loggerMiddleware())
		r.Use(brokerMiddleware(b))

		initializeRoutes(r)

		if err := r.Run(fmt.Sprintf("%s:%d", bindAddress, bindPort)); err != nil {
			panic(err)
		}
	}()

	<-ctx.Done()
	log.Debug("exiting web application")
}
