package service

import (
	"testing"
)

func TestValidate(t *testing.T) {
	cases := []struct {
		request string
		reply   []string
		want    interface{}
	}{
		{
			request: "validate-empty-reply",
			reply:   []string{},
			want:    "601: didn't receive expected response",
		},
		{
			request: "validate-reply",
			reply:   []string{"multi", "part", "is", "valid"},
			want:    "602: multiple replies received, only the first was handled",
		},
	}

	for _, tc := range cases {
		t.Run(tc.request, func(t *testing.T) {
			actual := validate(tc.request, tc.reply)
			if actual != tc.want && actual != nil {
				if actual.Error() != tc.want {
					t.Errorf("Error actual = %v, and Expected = %v.", actual, tc.want)
				}
			}
		})
	}
}
