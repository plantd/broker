package service

import (
	"context"
	"sync"

	gcontext "gitlab.com/plantd/broker/broker/context"

	log "github.com/sirupsen/logrus"
)

// MdpService defines an instance of the MDP request handling service.
//
// This service isn't a fully-formed idea yet, the existing gRPC service is
// meant to proxy calls to MDP but we don't need to do that to go from MDP
// to MDP. It would make the most sense here to create a service class with
// a worker pool where the workers connect to this broker and are handling
// broker specific requests over MDP. This should probably be removed until
// it's actually needed, but it doesn't do anything and is never created so
// for now it's being left in.
type MdpService struct {
	name     string
	endpoint *MdpEndpoint
	config   *gcontext.Config
}

// NewMdpService creates an instance of the service class.
func NewMdpService(name string, config *gcontext.Config) (*MdpService, error) {
	endpoint := NewMdpEndpoint(name, config.ClientEndpoint)

	return &MdpService{
		name,
		endpoint,
		config,
	}, nil
}

// Start launches the service.
func (s *MdpService) Start(ctx context.Context, wg *sync.WaitGroup) error {
	defer wg.Done()

	s.endpoint.Start(ctx)

	log.Debug("waiting for service shutdown")
	<-ctx.Done()
	log.Debug("exiting service")
	return nil
}
