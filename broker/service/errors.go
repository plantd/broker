package service

import "fmt"

const (
	// UnexpectedResponseCode is used with validation when the response has an error
	UnexpectedResponseCode = int32(iota + 601)

	// MultipleRepliesCode is a warning code until multiple replies are handled
	MultipleRepliesCode

	// UnmarshalFailedCode should be set when response data can't be unmarshalled
	UnmarshalFailedCode
)

// ResponseError is used to assign error codes to responses
type ResponseError struct {
	Code    int32
	Message string
}

// Error writes the code and message into a string
func (e ResponseError) Error() string {
	return fmt.Sprintf("%v: %v", e.Code, e.Message)
}

// UnexpectedResponseError creates an instance of the error class for an
// unexpected response.
func UnexpectedResponseError() ResponseError {
	return ResponseError{
		Code:    UnexpectedResponseCode,
		Message: "didn't receive expected response",
	}
}

// MultipleRepliesError creates an instance of the error class for a request
// response that received multiple replies.
func MultipleRepliesError() ResponseError {
	return ResponseError{
		Code:    MultipleRepliesCode,
		Message: "multiple replies received, only the first was handled",
	}
}

// UnmarshalFailedError creates an instance of the error class to use when
// the unmarshalling process has failed.
func UnmarshalFailedError() ResponseError {
	return ResponseError{
		Code:    UnmarshalFailedCode,
		Message: "failed to unmarshal response data",
	}
}
