package service

import (
	"bytes"
	"reflect"
	"sync"

	"gitlab.com/plantd/broker/pkg/mdp"
	pb "gitlab.com/plantd/broker/pkg/proto/v1"

	"github.com/golang/protobuf/jsonpb" //nolint
	"github.com/golang/protobuf/proto"  //nolint
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

// GrpcEndpoint defines a service that handles gRPC messages.
type GrpcEndpoint struct {
	name   string
	client Client
	mutex  *sync.Mutex
}

// NewGrpcEndpoint creates an instance of the endpoint class.
func NewGrpcEndpoint(name string, client *mdp.Client) (*GrpcEndpoint, error) {
	var c Client = client
	return &GrpcEndpoint{
		name:   name,
		client: c,
		mutex:  &sync.Mutex{},
	}, nil
}

// serialize a protobuf message.
func serialize(request interface{}) (b bytes.Buffer, err error) {
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err = marshaler.Marshal(&b, request.(proto.Message)); err != nil {
		log.WithFields(log.Fields{"err": err}).Error("jsonpb marshal")
	}
	return
}

// deserialize a reply into a protobuf message
func deserialize(reply []string, response interface{}) error {
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response.(proto.Message)); err != nil {
		log.WithFields(log.Fields{"err": err}).Error("jsonpb unmarshal")
		return err
	}
	return nil
}

// setResponseError checks if the response message has an error field and sets
// the code and message if it does.
func setResponseError(response interface{}, err ResponseError) {
	value := reflect.Indirect(reflect.ValueOf(response))
	field := value.FieldByName("Error")
	if field.IsValid() && field.CanSet() {
		sf := reflect.New(reflect.TypeOf(pb.Error{}))
		sf.Elem().FieldByName("Code").SetInt(int64(err.Code))
		sf.Elem().FieldByName("Message").SetString(err.Message)
		field.Set(sf)
	}
}

// sendMessage sends the serialized protobuf message and returns the message
// that was received as a reply.
func (e *GrpcEndpoint) sendMessage(request []string) ([]string, error) {
	log.WithFields(log.Fields{"request": request[0]}).Debug(request[1])

	// send the request
	err := e.client.Send(e.name, request...)
	if err != nil {
		log.WithFields(log.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	// wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		log.WithFields(log.Fields{"request": request[0]}).Error(err)
		return reply, err
	}

	log.WithFields(log.Fields{"request": request[0]}).Debugf("response: %v", reply)

	return reply, nil
}

func (e *GrpcEndpoint) handleMessage(message string, in interface{}, response interface{}) error {
	request := make([]string, 2)
	request[0] = message

	b, err := serialize(in)
	if err != nil {
		return err
	}
	request[1] = b.String()

	e.mutex.Lock()
	reply, err := e.sendMessage(request)
	if err != nil {
		return err
	}
	e.mutex.Unlock()

	if err = validate(request[0], reply); err != nil {
		return err
	}
	if err = deserialize(reply, response); err != nil {
		return err
	}

	return nil
}

// GetConfiguration <- ConfigurationRequest -> ConfigurationResponse
func (e *GrpcEndpoint) GetConfiguration(_ context.Context, in *pb.ConfigurationRequest) (*pb.ConfigurationResponse, error) {
	response := &pb.ConfigurationResponse{}
	if err := e.handleMessage("get-configuration", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetUnitConfiguration <- Empty -> ConfigurationResponse
func (e *GrpcEndpoint) GetUnitConfiguration(_ context.Context, in *pb.Empty) (*pb.ConfigurationResponse, error) {
	response := &pb.ConfigurationResponse{}
	if err := e.handleMessage("get-unit-configuration", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetStatus <- StatusRequest -> StatusResponse
func (e *GrpcEndpoint) GetStatus(_ context.Context, in *pb.StatusRequest) (*pb.StatusResponse, error) {
	response := &pb.StatusResponse{}
	if err := e.handleMessage("get-status", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetSettings <- SettingsRequest -> SettingsResponse
func (e *GrpcEndpoint) GetSettings(_ context.Context, in *pb.SettingsRequest) (*pb.SettingsResponse, error) {
	response := &pb.SettingsResponse{}
	if err := e.handleMessage("get-settings", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetChannel <- ChannelRequest -> ChannelResponse
func (e *GrpcEndpoint) GetChannel(_ context.Context, in *pb.ChannelRequest) (*pb.ChannelResponse, error) {
	response := &pb.ChannelResponse{}
	if err := e.handleMessage(GetChannel, in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetChannels <- Empty -> ChannelsResponse
func (e *GrpcEndpoint) GetChannels(_ context.Context, in *pb.Empty) (*pb.ChannelsResponse, error) {
	response := &pb.ChannelsResponse{}
	if err := e.handleMessage("get-channels", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetJob <- JobRequest -> JobResponse
func (e *GrpcEndpoint) GetJob(_ context.Context, in *pb.JobRequest) (*pb.JobResponse, error) {
	response := &pb.JobResponse{}
	if err := e.handleMessage("get-job", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetJobs <- Empty -> JobsResponse
func (e *GrpcEndpoint) GetJobs(_ context.Context, in *pb.Empty) (*pb.JobsResponse, error) {
	response := &pb.JobsResponse{}
	if err := e.handleMessage("get-jobs", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetJobStatus <- JobRequest -> JobStatusResponse
func (e *GrpcEndpoint) GetJobStatus(_ context.Context, in *pb.JobRequest) (*pb.JobStatusResponse, error) {
	response := &pb.JobStatusResponse{}
	if err := e.handleMessage("get-job-status", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetModule <- ModuleRequest -> ModuleResponse
func (e *GrpcEndpoint) GetModule(_ context.Context, in *pb.ModuleRequest) (*pb.ModuleResponse, error) {
	response := &pb.ModuleResponse{}
	if err := e.handleMessage("get-module", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetModules <- Empty -> ModulesResponse
func (e *GrpcEndpoint) GetModules(_ context.Context, in *pb.Empty) (*pb.ModulesResponse, error) {
	response := &pb.ModulesResponse{}
	if err := e.handleMessage("get-modules", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// TryModuleConnect <- ModuleRequest -> JobResponse
func (e *GrpcEndpoint) TryModuleConnect(_ context.Context, in *pb.ModuleRequest) (*pb.JobResponse, error) {
	response := &pb.JobResponse{}
	if err := e.handleMessage("try-module-connect", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// TryModuleDisconnect <- ModuleRequest -> JobResponse
func (e *GrpcEndpoint) TryModuleDisconnect(_ context.Context, in *pb.ModuleRequest) (*pb.JobResponse, error) {
	response := &pb.JobResponse{}
	if err := e.handleMessage("try-module-disconnect", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// TryModuleLoad <- ModuleRequest -> JobResponse
func (e *GrpcEndpoint) TryModuleLoad(_ context.Context, in *pb.ModuleRequest) (*pb.JobResponse, error) {
	response := &pb.JobResponse{}
	if err := e.handleMessage("try-module-load", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// TryModuleUnload <- ModuleRequest -> JobResponse
func (e *GrpcEndpoint) TryModuleUnload(_ context.Context, in *pb.ModuleRequest) (*pb.JobResponse, error) {
	response := &pb.JobResponse{}
	if err := e.handleMessage("try-module-unload", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// TryModuleReload <- ModuleRequest -> JobResponse
func (e *GrpcEndpoint) TryModuleReload(_ context.Context, in *pb.ModuleRequest) (*pb.JobResponse, error) {
	response := &pb.JobResponse{}
	if err := e.handleMessage("try-module-reload", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// TryModuleEnable <- ModuleRequest -> JobResponse
func (e *GrpcEndpoint) TryModuleEnable(_ context.Context, in *pb.ModuleRequest) (*pb.JobResponse, error) {
	response := &pb.JobResponse{}
	if err := e.handleMessage("try-module-enable", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// TryModuleDisable <- ModuleRequest -> JobResponse
func (e *GrpcEndpoint) TryModuleDisable(_ context.Context, in *pb.ModuleRequest) (*pb.JobResponse, error) {
	response := &pb.JobResponse{}
	if err := e.handleMessage("try-module-disable", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetModuleConfiguration <- ModuleRequest -> ConfigurationResponse
func (e *GrpcEndpoint) GetModuleConfiguration(_ context.Context, in *pb.ModuleRequest) (*pb.ConfigurationResponse, error) {
	response := &pb.ConfigurationResponse{}
	if err := e.handleMessage("get-module-configuration", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetModuleStatus <- ModuleRequest -> StatusResponse
func (e *GrpcEndpoint) GetModuleStatus(_ context.Context, in *pb.ModuleRequest) (*pb.StatusResponse, error) {
	response := &pb.StatusResponse{}
	if err := e.handleMessage("get-module-status", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetModuleSettings <- ModuleRequest -> SettingsResponse
func (e *GrpcEndpoint) GetModuleSettings(_ context.Context, in *pb.ModuleRequest) (*pb.SettingsResponse, error) {
	response := &pb.SettingsResponse{}
	if err := e.handleMessage("get-module-settings", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetModuleJob <- ModuleRequest -> JobResponse
func (e *GrpcEndpoint) GetModuleJob(_ context.Context, in *pb.ModuleJobRequest) (*pb.JobResponse, error) {
	response := &pb.JobResponse{}
	if err := e.handleMessage("get-module-job", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetModuleJobs <- ModuleRequest -> JobsResponse
func (e *GrpcEndpoint) GetModuleJobs(_ context.Context, in *pb.ModuleRequest) (*pb.JobsResponse, error) {
	response := &pb.JobsResponse{}
	if err := e.handleMessage("get-module-jobs", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetModuleActiveJob <- ModuleRequest -> JobResponse
func (e *GrpcEndpoint) GetModuleActiveJob(_ context.Context, in *pb.ModuleRequest) (*pb.JobResponse, error) {
	response := &pb.JobResponse{}
	if err := e.handleMessage("get-module-active-job", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// ModuleCancelJob <- ModuleJobRequest -> JobResponse
func (e *GrpcEndpoint) ModuleCancelJob(_ context.Context, in *pb.ModuleJobRequest) (*pb.JobResponse, error) {
	response := &pb.JobResponse{}
	if err := e.handleMessage("module-cancel-job", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// ModuleSubmitJob <- ModuleJobRequest -> JobResponse
func (e *GrpcEndpoint) ModuleSubmitJob(_ context.Context, in *pb.ModuleJobRequest) (*pb.JobResponse, error) {
	response := &pb.JobResponse{}
	if err := e.handleMessage("module-submit-job", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// ModuleSubmitEvent <- ModuleEventRequest -> JobResponse
func (e *GrpcEndpoint) ModuleSubmitEvent(_ context.Context, in *pb.ModuleEventRequest) (*pb.JobResponse, error) {
	response := &pb.JobResponse{}
	if err := e.handleMessage("module-submit-event", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// ModuleAvailableEvents <- ModuleRequest -> EventsResponse
func (e *GrpcEndpoint) ModuleAvailableEvents(_ context.Context, in *pb.ModuleRequest) (*pb.EventsResponse, error) {
	response := &pb.EventsResponse{}
	if err := e.handleMessage("module-available-events", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetModuleProperty <- PropertyRequest -> PropertyResponse
func (e *GrpcEndpoint) GetModuleProperty(_ context.Context, in *pb.PropertyRequest) (*pb.PropertyResponse, error) {
	response := &pb.PropertyResponse{}
	if err := e.handleMessage("get-module-property", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// SetModuleProperty <- PropertyRequest -> PropertyResponse
func (e *GrpcEndpoint) SetModuleProperty(_ context.Context, in *pb.PropertyRequest) (*pb.PropertyResponse, error) {
	response := &pb.PropertyResponse{}
	if err := e.handleMessage("set-module-property", in, response); err != nil {
		return nil, err
	}
	return response, nil
}

// GetModuleProperties <- PropertiesRequest -> PropertiesResponse
func (e *GrpcEndpoint) GetModuleProperties(_ context.Context, in *pb.PropertiesRequest) (*pb.PropertiesResponse, error) {
	request := make([]string, 2)
	request[0] = "get-module-properties"
	response := &pb.PropertiesResponse{}

	b, err := serialize(in)
	if err != nil {
		return nil, err
	}
	request[1] = b.String()

	e.mutex.Lock()
	reply, err := e.sendMessage(request)
	if err != nil {
		setResponseError(response, ResponseError{Code: 600, Message: err.Error()})
		return response, err
	}
	e.mutex.Unlock()

	if err = validate(request[0], reply); err != nil {
		setResponseError(response, err.(ResponseError))
		if err.(ResponseError).Code == UnexpectedResponseCode {
			return response, err
		}
	}
	if err = deserialize(reply, response); err != nil {
		setResponseError(response, UnmarshalFailedError())
		return response, err
	}

	return response, nil
}

// SetModuleProperties <- PropertiesRequest -> PropertiesResponse
func (e *GrpcEndpoint) SetModuleProperties(_ context.Context, in *pb.PropertiesRequest) (*pb.PropertiesResponse, error) {
	request := make([]string, 2)
	request[0] = "set-module-properties"
	response := &pb.PropertiesResponse{}

	b, err := serialize(in)
	if err != nil {
		return nil, err
	}
	request[1] = b.String()

	e.mutex.Lock()
	reply, err := e.sendMessage(request)
	if err != nil {
		setResponseError(response, ResponseError{Code: 600, Message: err.Error()})
		return response, err
	}
	e.mutex.Unlock()

	if err = validate(request[0], reply); err != nil {
		setResponseError(response, err.(ResponseError))
		if err.(ResponseError).Code == UnexpectedResponseCode {
			return response, err
		}
	}
	if err = deserialize(reply, response); err != nil {
		setResponseError(response, UnmarshalFailedError())
		return response, err
	}

	return response, nil
}
