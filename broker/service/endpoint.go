package service

import (
	"context"

	log "github.com/sirupsen/logrus"
)

// Client interface to wrap the MDP client to be able to mock it in tests.
type Client interface {
	Send(name string, request ...string) error
	Recv() ([]string, error)
}

// Endpoint interface to expose common methods to implement.
type Endpoint interface {
	GetChannel(_ context.Context, in interface{}) (interface{}, error)
	GetChannels(_ context.Context, in interface{}) (interface{}, error)
	GetConfiguration(_ context.Context, in interface{}) (interface{}, error)
	GetJob(_ context.Context, in interface{}) (interface{}, error)
	GetJobs(_ context.Context, in interface{}) (interface{}, error)
	GetJobStatus(_ context.Context, in interface{}) (interface{}, error)
	GetModule(_ context.Context, in interface{}) (interface{}, error)
	GetModules(_ context.Context, in interface{}) (interface{}, error)
	GetModuleActiveJob(_ context.Context, in interface{}) (interface{}, error)
	GetModuleJob(_ context.Context, in interface{}) (interface{}, error)
	GetModuleJobs(_ context.Context, in interface{}) (interface{}, error)
	GetModuleProperty(_ context.Context, in interface{}) (interface{}, error)
	GetModuleProperties(_ context.Context, in interface{}) (interface{}, error)
	GetModuleSettings(_ context.Context, in interface{}) (interface{}, error)
	GetModuleStatus(_ context.Context, in interface{}) (interface{}, error)
	GetStatus(_ context.Context, in interface{}) (interface{}, error)
	GetSettings(_ context.Context, in interface{}) (interface{}, error)
	GetUnitConfiguration(_ context.Context, in interface{}) (interface{}, error)
	ModuleSubmitJob(_ context.Context, in interface{}) (interface{}, error)
	ModuleSubmitEvent(_ context.Context, in interface{}) (interface{}, error)
	ModuleAvailableEvents(_ context.Context, in interface{}) (interface{}, error)
	SetModuleProperty(_ context.Context, in interface{}) (interface{}, error)
	SetModuleProperties(_ context.Context, in interface{}) (interface{}, error)
	TryModuleConnect(_ context.Context, in interface{}) (interface{}, error)
	TryModuleDisable(_ context.Context, in interface{}) (interface{}, error)
	TryModuleLoad(_ context.Context, in interface{}) (interface{}, error)
	TryModuleReload(_ context.Context, in interface{}) (interface{}, error)
	TryModuleUnload(_ context.Context, in interface{}) (interface{}, error)
}

// validate the reply that was received.
func validate(request string, reply []string) error {
	// validate the received message frame
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		err := UnexpectedResponseError()
		log.WithFields(log.Fields{"request": request}).Error(err)
		return err
	} else if len(reply) > 1 {
		err := MultipleRepliesError()
		log.WithFields(log.Fields{"request": request}).Warn(err)
		return err
	}
	return nil
}
