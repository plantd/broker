package service

import (
	"context"
	"fmt"
	"net"
	"sync"

	gcontext "gitlab.com/plantd/broker/broker/context"
	pb "gitlab.com/plantd/broker/pkg/proto/v1"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

// GrpcService defines an instance of the request relay service.
type GrpcService struct {
	name     string
	endpoint *GrpcEndpoint
	config   *gcontext.Config
}

// NewGrpcService creates an instance of the service class.
func NewGrpcService(name string, config *gcontext.Config) (service *GrpcService, err error) {
	client, err := gcontext.ConnectClient(config)
	if err != nil {
		log.WithFields(log.Fields{
			"unit": name,
			"err":  err,
		}).Error("failed to create mdp client")
		return nil, err
	}

	endpoint, err := NewGrpcEndpoint(name, client)
	if err != nil {
		log.WithFields(log.Fields{
			"unit": name,
			"err":  err,
		}).Error("failed to create unit endpoint connection")
		return nil, err
	}

	// initialize service client / service
	service = &GrpcService{
		name,
		endpoint,
		config,
	}

	return service, nil
}

// Start launches the service.
func (s *GrpcService) Start(ctx context.Context, wg *sync.WaitGroup) error {
	defer wg.Done()

	// Create the channel to listen on
	addr := fmt.Sprintf("%s:%d", s.config.Units[s.name].RPCBind, s.config.Units[s.name].RPCPort)
	log.WithFields(log.Fields{"endpoint": addr}).Debug("bind service")
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Error("failed to listen")
		return err
	}

	go func() {
		var opts []grpc.ServerOption

		log.WithFields(log.Fields{"service": s.name}).Debug("start gRPC server")

		// Start the gRPC endpoint
		srv := grpc.NewServer(opts...)
		pb.RegisterEndpointServer(srv, s.endpoint)
		reflection.Register(srv)

		if err = srv.Serve(lis); err != nil {
			log.WithFields(log.Fields{"err": err}).Error("failed to serve")
			panic("an unrecoverable error occurred")
		}
	}()

	log.Debug("waiting for service shutdown")
	<-ctx.Done()
	log.Debug("exiting service")
	return nil
}
