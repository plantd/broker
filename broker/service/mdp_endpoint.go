package service

import (
	"context"

	"gitlab.com/plantd/broker/pkg/mdp"

	log "github.com/sirupsen/logrus"
)

const poolSize = 3

// MdpEndpoint defines a service that handles ZeroMQ MDP messages.
type MdpEndpoint struct {
	name string
	pool []*mdp.Worker
}

// NewMdpEndpoint creates an instance of the endpoint class.
func NewMdpEndpoint(name string, endpoint string) *MdpEndpoint {
	pool := make([]*mdp.Worker, poolSize)
	for i := range pool {
		worker, err := mdp.NewWorker(endpoint, name)
		if err != nil {
			log.Panic("failed to create worker")
			return nil
		}

		pool[i] = worker
	}

	return &MdpEndpoint{
		name: name,
		pool: pool,
	}
}

func (e *MdpEndpoint) Start(ctx context.Context) {
	for _, worker := range e.pool {
		go e.startWorker(ctx, worker)
	}
}

func (e *MdpEndpoint) startWorker(ctx context.Context, worker *mdp.Worker) {
	defer worker.Close()

	go func() {
		var reply []string
		for {
			log.Debug("waiting for request")
			request, err := worker.Recv(reply)
			if err != nil {
				log.Errorf("%s", err)
			}
			log.Debugf("request received: %v\n", request)

			if len(request) == 0 {
				log.Errorf("request has zero parts")
				continue
			}

			reply, err := e.HandleRequest(ctx, request)
			if err != nil {
				log.WithFields(log.Fields{"error": err}).Error("error while handling request")
			}
			log.WithFields(log.Fields{"reply": reply}).Debug("received response")
		}
	}()

	<-ctx.Done()
	log.Println("stopping worker")
}

func (e *MdpEndpoint) HandleRequest(ctx context.Context, request []string) ([]string, error) {
	// Just echoing for now, when an actual implementation is needed see the e2e example
	log.WithFields(log.Fields{"request": request}).Debug("handling request")
	return request, nil
}
