M := $(shell printf "\033[34;1m▶\033[0m")

examples: \
	ex/zapi/bus \
	ex/zapi/broker \
	ex/zapi/client \
	ex/zapi/worker \
	; $(info $(M) Building examples...)

ex/zapi/bus:
	@go build -o target/zapi-bus ./examples/zapi/bus/main.go

ex/zapi/broker:
	@go build -o target/zapi-broker ./examples/zapi/broker/main.go

ex/zapi/client:
	@go build -o target/zapi-client ./examples/zapi/client/main.go

ex/zapi/worker:
	@go build -o target/zapi-worker ./examples/zapi/worker/main.go

.PHONY: examples
