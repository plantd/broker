[![Build Status](https://gitlab.com/plantd/broker/badges/staging/pipeline.svg)](https://gitlab.com/plantd/broker/-/commits/staging)
[![Coverage Report](https://gitlab.com/plantd/broker/badges/staging/coverage.svg)](https://gitlab.com/plantd/broker/commits/staging)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/plantd/broker)](https://goreportcard.com/report/gitlab.com/plantd/broker)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

---

# Plantd Broker

This is beta software and is subject to API changes.

The message broker service is responsible for taking gRPC requests and
converting them into equivalent ZeroMQ ones that are handled by services
connected as workers.

## Quick Start

To try things out without all the setup:

```shell
yarn --cwd app/ install
yarn --cwd app/ build
docker-compose up
```

The UI should be available at http://localhost, the API endpoint at [localhost:9080/api/v1](), and the broker endpoint
at [tcp://localhost:7200]().

## Setup

### Dependencies

ZeroMQ/GoMQ has instructions [here][gomq] on a setup that requires

* libsodium
* libzmq
* czmq

In Arch these can be installed by

```sh
pacman -S libsodium libzmq
git clone https://aur.archlinux.org/czmq.git
cd czmq
makepkg -si
```

### Building

#### Broker

Build the gRPC service and messages using the protobuf repo.

```sh
cd broker
make
```

#### Admin Interface

```shell
cd app
yarn install
yarn build
```

### Publishing

```sh
tag="v1"
case "$(git rev-parse --abbrev-ref HEAD)" in
  "staging") tag="latest" ;;
  "main")    tag="stable" ;;
  "*")       tag=$tag     ;;
esac
docker build -t registry.gitlab.com/plantd/broker:$tag -f ./build/Dockerfile .
docker push registry.gitlab.com/plantd/broker:$tag
```

## Roadmap

* [ ] Move workers out
  > The worker processes are currently within this service, but they do not need
    to be. These should exist in the associated services, and connect to this
    broker.
* [ ] Implement the titanic pattern device
* [ ] Implement load balancing
* [ ] Add Curve for ZeroMQ security
* [x] Move pubsub bus from other service into this one

<!-- Links -->
[gomq]: https://github.com/zeromq/gomq
